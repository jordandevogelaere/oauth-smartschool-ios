//
//  ViewController.swift
//  oAuthSMSC
//
//  Created by Jordan de Vogelaere on 03/06/2019.
//  Copyright © 2019 Jordan de Vogelaere. All rights reserved.
//

import UIKit
import SafariServices
import CommonCrypto


struct DomainVerification: Decodable {
    var isValid: Bool
}

struct SMSCAuth: Decodable {
    var token_type: String
    var expires_in: Int
    var access_token: String
    var refresh_token: String
}

class ViewController: UIViewController {
    
    var domain: String? = nil
    
    lazy var domainTextField: UITextField = {
        let view = UITextField()
        view.placeholder = "Domein"
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }()
    
    lazy var loginButton: UIButton = {
        let view = UIButton()
        view.backgroundColor = .orange
        view.setTitle("Login", for: .normal)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.widthAnchor.constraint(equalToConstant: 100).isActive = true
        view.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        return view
    }()
    
    lazy var myStackView: UIStackView = {
        let view = UIStackView()
        view.axis = .vertical
        view.spacing = 20
        view.alignment = .fill
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = .white
        loginButton.addTarget(self, action: #selector(didTapLogin), for: .touchUpInside)
        myStackView.addArrangedSubview(domainTextField)
        myStackView.addArrangedSubview(loginButton)
        view.addSubview(myStackView)
        
        myStackView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 30).isActive = true
        myStackView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 16).isActive = true
        myStackView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -16).isActive = true
        
    }
    
    @objc private func didTapLogin() {
        let decoder = JSONDecoder()
        if let domainTextFieldText = domainTextField.text {
            domain = domainTextFieldText
            let fullUrl = "https://\(domainTextFieldText).smartschool.be/OAuth/verifyplatform"
            
            let url = URL(string: fullUrl)
            
            let task = URLSession.shared.dataTask(with: url!) {(data, response, error) in
                guard let data = data else { return }
                do {
                    let isValidDomain = try decoder.decode(DomainVerification.self, from: data)
                    if isValidDomain.isValid {
                        self.startOAuthFlow()
                    }
                } catch {
                    print(error.localizedDescription)
                }
            }
            
            task.resume()
        }
    }
    
    private func startOAuthFlow() {
        DispatchQueue.main.async {
            let verifier = self.generateCodeVerifier()
            let codeChallenge = self.codeChallenge(with: verifier)
            let urlString = "https://\(self.domain!).smartschool.be/OAuth/mobile?client_id=123&response_type=code&scope=mobile&code_challenge=\(codeChallenge)&code_challenge_method=S256&redirect_uri=smsc://"
                        let url = URL(string: urlString )
            if let oAuthUrl = url {
                UIApplication.shared.open(oAuthUrl)
            }
        }
    }
    
    func generateCodeVerifier() -> String {
        var buffer = [UInt8](repeating: 0, count: 32)
        _ = SecRandomCopyBytes(kSecRandomDefault, buffer.count, &buffer)
        let verifier = Data(bytes: buffer).base64EncodedString()
            .replacingOccurrences(of: "+", with: "-")
            .replacingOccurrences(of: "/", with: "_")
            .replacingOccurrences(of: "=", with: "")
            .trimmingCharacters(in: .whitespaces)
        
        return verifier
    }
    
    private func codeChallenge(with verifier: String) -> String {
        let codeVerifierAppDelegate = UIApplication.shared.delegate as! AppDelegate
        codeVerifierAppDelegate.code_verifier = verifier
        guard let data = verifier.data(using: .utf8) else { return "" }
        var buffer = [UInt8](repeating: 0,  count: Int(CC_SHA256_DIGEST_LENGTH))
        data.withUnsafeBytes {
            _ = CC_SHA256($0, CC_LONG(data.count), &buffer)
        }
        let hash = Data(bytes: buffer)
        let challenge = hash.base64EncodedString()
            .replacingOccurrences(of: "+", with: "-")
            .replacingOccurrences(of: "/", with: "_")
            .replacingOccurrences(of: "=", with: "")
            .trimmingCharacters(in: .whitespaces)
        
        return challenge
    }

}
